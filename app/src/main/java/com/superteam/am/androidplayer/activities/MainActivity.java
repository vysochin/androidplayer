package com.superteam.am.androidplayer.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.superteam.am.androidplayer.R;
import com.superteam.am.androidplayer.entities.PlayerSong;
import com.superteam.am.androidplayer.entities.StaticContainer;
import com.superteam.am.androidplayer.fragments.AbstractFragment;
import com.superteam.am.androidplayer.fragments.AlbumsFragment;
import com.superteam.am.androidplayer.fragments.AllCompositionsFragment;
import com.superteam.am.androidplayer.fragments.HomeFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Denys Vysochin on 29.09.2016.
 */
public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    /** Fragments */
    private AlbumsFragment albumsFragment;
    private AllCompositionsFragment allCompositionsFragment;
    private HomeFragment homeFragment;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.main_template_layout);
        init();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }



    public void onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.item_home:
                replaceFragment(homeFragment);
                break;
            case R.id.item_all_compositions:
                replaceFragment(allCompositionsFragment);

                break;
/*            case R.id.item_albums:
                replaceFragment(albumsFragment);
                break;*/
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.menu_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void replaceFragment(AbstractFragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (fragmentManager.getFragments() != null) {
            fragmentTransaction.replace(R.id.main_container, fragment);
        } else {
            fragmentTransaction.add(R.id.main_container, fragment);
        }
        fragmentTransaction.commit();
    }


    private void init() {
        fragmentManager = getSupportFragmentManager();

        /** Fragments initialization */
        albumsFragment = new AlbumsFragment();
        allCompositionsFragment = new AllCompositionsFragment();
        homeFragment = new HomeFragment();

        replaceFragment(homeFragment);
    }

    /** All compositions handlers*/


}
