package com.superteam.am.androidplayer.entities;

import android.media.MediaPlayer;

import java.util.ArrayList;

/**
 * Created by admin-pc on 11.12.2016.
 */
public class StaticContainer {

    public static ArrayList<PlayerSong> playerSongs = new ArrayList<>();

    public static ArrayList<PlayerSong> shuffledList = new ArrayList<>();

    public static MediaPlayer mediaPlayer;

}
