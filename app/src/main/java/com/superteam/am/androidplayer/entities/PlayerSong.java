package com.superteam.am.androidplayer.entities;

import android.widget.SeekBar;

/**
 * Created by admin-pc on 11.12.2016.
 */
public class PlayerSong {

    private String name;

    private String path;

    private String time;

    public PlayerSong(String name, String path, String time) {
        this.name = name;
        this.path = path;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return getTime() + " - " + getName();
    }
}
