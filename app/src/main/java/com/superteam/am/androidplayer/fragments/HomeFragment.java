package com.superteam.am.androidplayer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superteam.am.androidplayer.R;

/**
 * Created by Denys Vysochin on 23.10.2016.
 */
public class HomeFragment extends AbstractFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_layout, container, false);
    }
}
