package com.superteam.am.androidplayer.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.superteam.am.androidplayer.R;
import com.superteam.am.androidplayer.entities.PlayerSong;
import com.superteam.am.androidplayer.entities.StaticContainer;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Denys Vysochin on 28.09.2016.
 */
public class PlayActivity extends Activity implements MediaPlayer.OnPreparedListener {

    SeekBar seekBar;

    TextView textView;

    TextView nameText;

    PlayProcess playProcess = new PlayProcess();

    int percentage;

    private String musicName = "";
    private String musicPath = "";
    private long musicId = 0;

    private boolean isRepeat;
    private boolean isShuffle;
    private boolean isEnd;

    boolean isPlayerStart;

    @Override
    public void onPrepared(MediaPlayer mp) {

    }

    private class PlayProcess extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (true) {
                try {
                    Thread.sleep(1000);
                    percentage = (int) (((double) StaticContainer.mediaPlayer.getCurrentPosition() / StaticContainer.mediaPlayer.getDuration()) * 100);
                    publishProgress();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            try {
                updateSeekBur();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        musicName = getIntent().getStringExtra("MUSIC_NAME");
        musicPath = getIntent().getStringExtra("MUSIC_PATH");
        musicId = getIntent().getLongExtra("MUSIC_ID", 0);
        setContentView(R.layout.play_layout);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        textView = (TextView) findViewById(R.id.textTest);
        nameText = (TextView) findViewById(R.id.song_name);
        nameText.setText(musicName);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    textView.setText(String.valueOf(seekBar.getProgress()));
                    StaticContainer.mediaPlayer.seekTo((int) (StaticContainer.mediaPlayer.getDuration() * ((double) seekBar.getProgress() / 100)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        runNewMusic(musicPath);
    }

    @Override
    protected void onPause() {
        StaticContainer.mediaPlayer.pause();
        super.onPause();
    }

    public void onPlayStart(View view) {
        try {
            StaticContainer.mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onPlayPause(View view) {
        StaticContainer.mediaPlayer.pause();
    }

    public void onNext(View view) {
        nextSong();
    }

    public void onPrevious(View view) {
        previousSong();
    }

    public void onShuffle(View view) {
        isShuffle = !isShuffle;
    }

    public void onRepeat(View view) {
        isRepeat = !isRepeat;
    }

    public void updateSeekBur() {
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        textView = (TextView) findViewById(R.id.textTest);
        seekBar.setProgress(percentage);
        textView.setText(String.valueOf(seekBar.getProgress())+"%");
    }

    public void runNewMusic(String musicPath) {
        try {
            nameText.setText(musicName);
            if (StaticContainer.mediaPlayer != null) {
                StaticContainer.mediaPlayer.release();
            }
            StaticContainer.mediaPlayer = new MediaPlayer();
            StaticContainer.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    isEnd = true;
                    playAfterEnd();
                }
            });
            StaticContainer.mediaPlayer.setDataSource(musicPath);
            StaticContainer.mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            StaticContainer.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    StaticContainer.mediaPlayer.start();
                }
            });
            StaticContainer.mediaPlayer.prepareAsync();
            if (!isPlayerStart) {
                playProcess.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                isPlayerStart = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void nextSong() {
        try {
            if (isShuffle) {
                StaticContainer.mediaPlayer.pause();
                if (StaticContainer.shuffledList.size() - 1 == musicId) {
                    musicId = 0;
                } else {
                    musicId++;
                }
                musicName = StaticContainer.shuffledList.get((int) musicId).getName();
                musicPath = StaticContainer.shuffledList.get((int) musicId).getPath();
                runNewMusic(musicPath);
            } else {
                StaticContainer.mediaPlayer.pause();
                if (StaticContainer.playerSongs.size() - 1 == musicId) {
                    musicId = 0;
                } else {
                    musicId++;
                }
                musicName = StaticContainer.playerSongs.get((int) musicId).getName();
                musicPath = StaticContainer.playerSongs.get((int) musicId).getPath();
                runNewMusic(musicPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void previousSong() {
        try {
            if (isShuffle) {
                StaticContainer.mediaPlayer.pause();
                if (0 == musicId) {
                    musicId = StaticContainer.shuffledList.size() - 1;
                } else {
                    musicId--;
                }
                musicName = StaticContainer.shuffledList.get((int) musicId).getName();
                musicPath = StaticContainer.shuffledList.get((int) musicId).getPath();
                runNewMusic(musicPath);
            } else {
                StaticContainer.mediaPlayer.pause();
                if (0 == musicId) {
                    musicId = StaticContainer.playerSongs.size() - 1;
                } else {
                    musicId--;
                }
                musicName = StaticContainer.playerSongs.get((int) musicId).getName();
                musicPath = StaticContainer.playerSongs.get((int) musicId).getPath();
                runNewMusic(musicPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playAfterEnd() {
        if (isEnd) {
            if (isRepeat) {
                runNewMusic(musicPath);
            } else if (isShuffle) {
                shuffle();
                nextSong();
            } else {
                nextSong();
            }
            isEnd = false;
        }
    }

    public void shuffle() {
        long seed = System.nanoTime();
        StaticContainer.shuffledList = (ArrayList<PlayerSong>) StaticContainer.playerSongs.clone();
        Collections.shuffle(StaticContainer.shuffledList, new Random(seed));
    }
}
