package com.superteam.am.androidplayer.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.superteam.am.androidplayer.R;
import com.superteam.am.androidplayer.activities.PlayActivity;
import com.superteam.am.androidplayer.entities.PlayerSong;
import com.superteam.am.androidplayer.entities.StaticContainer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Denys Vysochin on 29.09.2016.
 */
//TODO: Need implement
public class AllCompositionsFragment extends AbstractFragment{

    //public ArrayList<PlayerSong> playerSongs = new ArrayList<>();
    ProgressDialog progress;
    final String MEDIA_PATH = Environment.getExternalStorageDirectory()
            .getPath() + "/";
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<>();
    private String mp3Pattern = ".mp3";


    private ArrayList<PlayerSong> playerSongs = new ArrayList<>();
    private LoadMusicProcess loadMusicProcess = new LoadMusicProcess();

    boolean isLoadDone;


    private class LoadMusicProcess extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (true) {
                try {
                    createSongList();
                    publishProgress();
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            try {
                if (!isLoadDone) {
                    loadDone();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (StaticContainer.playerSongs.isEmpty()) {
            loadMusicProcess.execute();
        }
        return inflater.inflate(R.layout.all_compositions_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (StaticContainer.playerSongs.isEmpty()) {
            String loadTitle = getResources().getString(R.string.loading);
            String loadMessage = getResources().getString(R.string.music_loading);
            progress = new ProgressDialog(getContext());
            progress.setTitle(loadTitle);
            progress.setMessage(loadMessage);
            progress.setCancelable(false);
            progress.show();
        } else {
            loadDone();
        }
    }

    public void onMusicSelected(PlayerSong song, long id) {
        Intent intent = new Intent(getContext(), PlayActivity.class);
        intent.putExtra("MUSIC_NAME", song.getName());
        intent.putExtra("MUSIC_PATH", song.getPath());
        intent.putExtra("MUSIC_ID", id);
        startActivity(intent);
    }


    public void createSongList() {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        Uri uri;
        PlayerSong newSong;

        getPlayList();

        for(HashMap<String, String> song : songsList) {

            uri = Uri.parse(song.get("songPath"));
            mmr.setDataSource(getContext(), uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millSecond = Integer.parseInt(durationStr);
            String duration = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millSecond),
                    TimeUnit.MILLISECONDS.toSeconds(millSecond) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millSecond))
            );

            newSong = new PlayerSong(song.get("songTitle"), song.get("songPath"), duration);
            playerSongs.add(newSong);
        }
        StaticContainer.playerSongs = playerSongs;

    }

    public ArrayList<HashMap<String, String>> getPlayList() {
        System.out.println(MEDIA_PATH);
        if (MEDIA_PATH != null) {
            File home = new File(MEDIA_PATH);
            File[] listFiles = home.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    System.out.println(file.getAbsolutePath());
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addSongToList(file);
                    }
                }
            }
        }
        // return songs list array
        return songsList;
    }

    private void scanDirectory(File directory) {
        if (directory != null) {
            File[] listFiles = directory.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addSongToList(file);
                    }

                }
            }
        }
    }

    private void addSongToList(File song) {
        if (song.getName().endsWith(mp3Pattern)) {
            HashMap<String, String> songMap = new HashMap<>();
            songMap.put("songTitle",
                    song.getName().substring(0, (song.getName().length() - 4)));
            songMap.put("songPath", song.getPath());

            // Adding each song to SongList
            songsList.add(songMap);
        }
    }

    public void loadDone() {
        isLoadDone = true;
        progress.dismiss();
        ListView listView = (ListView) getView().findViewById(R.id.songsList);

        final ArrayAdapter<PlayerSong> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, StaticContainer.playerSongs);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlayerSong song = adapter.getItem(position);

                onMusicSelected(song, id);
            }
        });
        adapter.notifyDataSetChanged();
    }
}
