package com.superteam.am.androidplayer.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superteam.am.androidplayer.R;

/**
 * Created by Denys Vysochin on 29.09.2016.
 */
//TODO: Need implement
public class AlbumsFragment extends AbstractFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.albums_fragment, container, false);
    }
}
